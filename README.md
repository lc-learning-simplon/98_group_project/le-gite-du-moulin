# EFFECTUER UNE SAUVEGARDE

1. Ouvrir le terminal et dirigez vous vers le serveur `ssh user@IP`.
2. Entrez le mot de passe associé à l'utilisateur.
3. Dirigez vous vers le dossier parent au dossier `www`.
4. Sauvegardez la base de donnée `mysqldump -u user -p --opt bddname > save.sql`.
5. Entrez le mot de passe associé à la base de donnée.
6. Votre base de donnée est sauvegardé au format `.sql` !
7. `git add .`
8. `git commit`
9. `git push`

## RECUPERER UNE SAUVEGARDE
`mysql bddname < save.sql -p`


# CREER UN  THEME WORDPRESS ENFANT


Permet d'avoir une sécurité si une mise à jour majeur de thème utilisé peut casser votre utilisation du thème.


## PROTOCOLE :

1. Créer un fichier "function.php" :

```php
<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

?>
```

2. Créer un fichier "style.css" (En remplaçant les informations par les vôtres) :

```css
/*
Theme Name: full-frame-child
Description: Une description de votre thème
Author: Cécile Elodie Lucas
Author URI: https://moulin.labege.srv.simplon.me
Template: full-frame
Version: 1.0
*/
```

3. Envoyer avec FileZilla ces deux script dans un nouveau dossier à l'emplacement `wp-content/themes/nom_du_theme-child`.

4. Allez sur Wordpresse et activer votre Thème-child !

5. Pour plus de peps ajoutez une image "screenshot.png" dans le dossier `nom_du_theme-child`


ENJOY !

